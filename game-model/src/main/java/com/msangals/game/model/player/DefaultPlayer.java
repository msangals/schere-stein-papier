package com.msangals.game.model.player;

import com.msangals.game.model.duel.Symbol;

/**
 * A default player plays the game with a default {@link Symbol}. The default
 * symbol is of type STEIN.
 * 
 * @author msangals
 *
 */
public class DefaultPlayer implements Player {

	private static final Symbol symbol = Symbol.STEIN;
	
	/**
	 * {@inheritDoc}
	 */
	public Symbol getSymbol() {
		return symbol;
	}

}
