package com.msangals.game.model.duel;

/**
 * The Round result enum is to define which result has a single played round.
 * 
 * @author msangals
 *
 */
public enum RoundResult {

	DRAW, DEFAULT_PLAYER, RANDOM_PLAYER;

}
