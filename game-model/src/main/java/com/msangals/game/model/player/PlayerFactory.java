package com.msangals.game.model.player;

/**
 * The main contract here is the creation of player instances.
 * 
 * @author msangals
 *
 */
public class PlayerFactory {

	/**
	 * Gets a random or default player depending on given type. If the given
	 * type not exists this method will be return null.
	 * 
	 * @param playerType
	 *            the player type
	 * @return a player, or null if the type not exists
	 */
	public Player getPlayer(PlayerType playerType) {

		if (playerType == null) {
			return null;
		}

		Player player;

		switch (playerType) {
		case DEFAULT:
			player = new DefaultPlayer();
			break;
		case RANDOM:
			player = new RandomPlayer();
			break;
		default:
			player = null;
		}

		return player;
	}
}
