package com.msangals.game.model.duel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Symbols to play the game. A player can get on every one symbol. Every symbol
 * has a different weighting.
 * <p>
 * The weighting is following:
 * </p>
 * <ul>
 * <li>SCHERE wins over PAPIER</li>
 * <li>STEIN wins over SCHERE</li>
 * <li>PAPIER wins over STEIN</li>
 * </ul>
 * 
 * @author msangals
 *
 */
public enum Symbol {
	SCHERE, STEIN, PAPIER;

	/**
	 * Gets a random symbol.
	 * 
	 * @return a random symbol.
	 */
	public static Symbol getRandom() {
		List<Symbol> values = Collections.unmodifiableList(Arrays
				.asList(values()));
		Random random = new Random();

		return values.get(random.nextInt(values.size()));
	}

}
