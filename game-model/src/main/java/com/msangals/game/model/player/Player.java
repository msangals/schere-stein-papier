package com.msangals.game.model.player;

import com.msangals.game.model.duel.Symbol;

/**
 * Player to play the game. On every round the player can get a symbol.
 * 
 * @author msangals
 *
 */
public interface Player {

	/**
	 * Gets a symbol to play.
	 * 
	 * @return the symbol
	 */
	Symbol getSymbol();
}
