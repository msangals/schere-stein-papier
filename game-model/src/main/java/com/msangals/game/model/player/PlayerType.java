package com.msangals.game.model.player;

/**
 * The different player types.
 * 
 * @author msangals
 *
 */
public enum PlayerType {
	RANDOM, DEFAULT;
}
