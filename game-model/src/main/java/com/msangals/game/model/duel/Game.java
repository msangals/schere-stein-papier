package com.msangals.game.model.duel;

import java.util.HashMap;
import java.util.Map;

import com.msangals.game.model.player.Player;

/**
 * Game model class. The initial state of game result will be set to 0 after
 * instantiate this class.
 * 
 * @author msangals
 *
 */
public class Game {

	public static final int ROUNDS_TO_PLAY = 100;

	private Player defaultPlayer;
	private Player randomPlayer;
	private Map<RoundResult, Integer> result;

	/**
	 * Constructs a new Game with the given players.
	 * 
	 * @param defaultPlayer
	 *            the default player
	 * @param randomPlayer
	 *            the random player
	 */
	public Game(Player defaultPlayer, Player randomPlayer) {
		this.defaultPlayer = defaultPlayer;
		this.randomPlayer = randomPlayer;
		result = new HashMap<>();
		result.put(RoundResult.DEFAULT_PLAYER, 0);
		result.put(RoundResult.RANDOM_PLAYER, 0);
		result.put(RoundResult.DRAW, 0);
	}

	/**
	 * Gets the default player.
	 * 
	 * @return the default player
	 */
	public Player getDefaultPlayer() {
		return defaultPlayer;
	}

	/**
	 * Sets the default player.
	 * 
	 * @param defaultPlayer
	 *            the default player to set
	 */
	public void setDefaultPlayer(Player defaultPlayer) {
		this.defaultPlayer = defaultPlayer;
	}

	/**
	 * Gets the random player.
	 * 
	 * @return the random player
	 */
	public Player getRandomPlayer() {
		return randomPlayer;
	}

	/**
	 * Sets the random player.
	 * 
	 * @param randomPlayer
	 *            the random player to set
	 */
	public void setRandomPlayer(Player randomPlayer) {
		this.randomPlayer = randomPlayer;
	}

	/**
	 * Gets the result.
	 * 
	 * @return the result
	 */
	public Map<RoundResult, Integer> getResult() {
		return result;
	}

	/**
	 * Sets the result.
	 * 
	 * @param result
	 *            the result to set
	 */
	public void setResult(Map<RoundResult, Integer> result) {
		this.result = result;
	}

}
