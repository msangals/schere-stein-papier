package com.msangals.game.model.player;

import com.msangals.game.model.duel.Symbol;

/**
 * A random player gets a random symbol to play.
 * 
 * @author msangals
 *
 */
public class RandomPlayer implements Player {

	/**
	 * {@inheritDoc}
	 */
	public Symbol getSymbol() {
		return Symbol.getRandom();
	}

}
