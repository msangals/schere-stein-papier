package com.msangals.game.model.duel;

/**
 * Round model.
 * 
 * @author msangals
 *
 */
public class Round {

	public static final String ROUND_PREFIX = "ROUND_%d";

	private Game game;
	private String symbolicName;
	private RoundResult result;

	private Symbol defaultPlayerSymbol;
	private Symbol randomPlayerSymbol;

	public Round(Game game, int roundNumber) {
		this.game = game;
		this.symbolicName = String.format(ROUND_PREFIX, roundNumber);
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getSymbolicName() {
		return symbolicName;
	}

	public void setSymbolicName(String symbolicName) {
		this.symbolicName = symbolicName;
	}

	public RoundResult getResult() {
		return result;
	}

	public void setResult(RoundResult result) {
		this.result = result;
	}

	public Symbol getDefaultPlayerSymbol() {
		return defaultPlayerSymbol;
	}

	public void decideDefaulPlayerSymbol() {
		this.defaultPlayerSymbol = game.getDefaultPlayer().getSymbol();
	}

	public void setDefaultPlayerSymbol(Symbol defaultPlayerSymbol) {
		this.defaultPlayerSymbol = defaultPlayerSymbol;
	}

	public Symbol getRandomPlayerSymbol() {
		return randomPlayerSymbol;
	}

	public void decideRandomPlayerSymbol() {
		this.randomPlayerSymbol = game.getRandomPlayer().getSymbol();
	}

	public void setRandomPlayerSymbol(Symbol randomPlayerSymbol) {
		this.randomPlayerSymbol = randomPlayerSymbol;
	}

}
