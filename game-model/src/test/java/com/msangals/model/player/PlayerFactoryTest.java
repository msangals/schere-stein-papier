package com.msangals.model.player;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.msangals.game.model.player.DefaultPlayer;
import com.msangals.game.model.player.Player;
import com.msangals.game.model.player.PlayerFactory;
import com.msangals.game.model.player.PlayerType;
import com.msangals.game.model.player.RandomPlayer;

/**
 * Unit tests for {@link PlayerFactory}.
 */
public class PlayerFactoryTest {

	private PlayerFactory playerFactory;

	@Before
	public void before() {
		playerFactory = new PlayerFactory();
	}

	@Test
	public void testGetRandomPlayer() {
		Player player = playerFactory.getPlayer(PlayerType.RANDOM);
		assertThat(player, instanceOf(RandomPlayer.class));
	}

	@Test
	public void testGetDefaultPlayer() {
		Player player = playerFactory.getPlayer(PlayerType.DEFAULT);
		assertThat(player, instanceOf(DefaultPlayer.class));
	}

	@Test
	public void testGetNonExistingPlayerType() {
		Player player = playerFactory.getPlayer(null);
		assertThat(player, is(nullValue()));
	}

}
