package com.msangals.model.player;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.msangals.game.model.duel.Symbol;
import com.msangals.game.model.player.RandomPlayer;

/**
 * Unit tests of {@link RandomPlayer}.
 * 
 * @author msangals
 *
 */
public class RandomPlayerTest {

	private RandomPlayer randomPlayer;

	@Before
	public void before() {
		randomPlayer = new RandomPlayer();
	}

	@Test
	public void testGetSymbol() {
		assertThat(randomPlayer.getSymbol(), instanceOf(Symbol.class));
	}

}
