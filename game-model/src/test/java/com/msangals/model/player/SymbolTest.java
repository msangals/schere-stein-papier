package com.msangals.model.player;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.msangals.game.model.duel.Symbol;

/**
 * Unit tests for {@link Symbol}.
 * 
 * @author msangals
 *
 */
public class SymbolTest {

	@Test
	public void getRandomSymbol() {
		assertThat(Symbol.getRandom(), instanceOf(Symbol.class));
	}

}
