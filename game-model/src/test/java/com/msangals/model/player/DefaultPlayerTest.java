package com.msangals.model.player;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.msangals.game.model.duel.Symbol;
import com.msangals.game.model.player.DefaultPlayer;

/**
 * Unit tests of {@link DefaultPlayer}.
 * 
 * @author msangals
 *
 */
public class DefaultPlayerTest {
	private DefaultPlayer defaultPlayer;

	@Before
	public void before() {
		defaultPlayer = new DefaultPlayer();
	}

	@Test
	public void testGetSymbol() {
		assertThat(defaultPlayer.getSymbol(), is(Symbol.STEIN));
	}
}
