package com.msangals.model.duel;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.msangals.game.model.duel.Game;
import com.msangals.game.model.duel.Round;
import com.msangals.game.model.player.PlayerFactory;
import com.msangals.game.model.player.PlayerType;

/**
 * Unit tests for {@link Round}.
 * 
 * @author msangals
 *
 */
public class RoundTest {

	@Test
	public void symbolicNameRoundTest() {
		PlayerFactory playerFactory = new PlayerFactory();
		Game game = new Game(playerFactory.getPlayer(PlayerType.DEFAULT),
				playerFactory.getPlayer(PlayerType.RANDOM));

		Round round = new Round(game, 0);

		assertThat(round.getSymbolicName(),
				is(String.format(Round.ROUND_PREFIX, 0)));
	}
}
