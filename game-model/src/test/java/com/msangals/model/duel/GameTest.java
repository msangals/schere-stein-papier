package com.msangals.model.duel;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.msangals.game.model.duel.Game;
import com.msangals.game.model.duel.RoundResult;
import com.msangals.game.model.player.PlayerFactory;
import com.msangals.game.model.player.PlayerType;

/**
 * Unit tests for {@link Game}.
 * 
 * @author msangals
 *
 */
public class GameTest {

	@Test
	public void initResultTest() {
		PlayerFactory playerFactory = new PlayerFactory();
		Game game = new Game(playerFactory.getPlayer(PlayerType.DEFAULT),
				playerFactory.getPlayer(PlayerType.RANDOM));

		assertThat(game.getResult().get(RoundResult.DRAW), is(0));
		assertThat(game.getResult().get(RoundResult.DEFAULT_PLAYER), is(0));
		assertThat(game.getResult().get(RoundResult.RANDOM_PLAYER), is(0));
	}

}
