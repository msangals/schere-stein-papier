package com.msangals.game.util.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;

import com.msangals.game.model.duel.Round;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/**
 * Class to storage data to CSV file.
 * 
 * @author msangals
 *
 */
public class CsvStorage {

	private static CsvStorage instance = null;
	private static final String FILENAME = "points.csv";

	private CsvStorage() {
		// Private constructor to ensure that no other instances can be created.
	}

	/**
	 * Creates a singleton instance of csv storage.
	 * 
	 * @return a singleton instance of csv storage
	 */
	public synchronized static CsvStorage getInstance() {
		if (instance == null) {
			instance = new CsvStorage();
		}
		return instance;
	}

	/**
	 * Reads all results from the csv file.
	 * 
	 * @return list of results
	 */
	public List<String[]> getResults() {

		List<String[]> results = new ArrayList<>();

		try {
			CSVReader reader = new CSVReader(new FileReader(FILENAME));
			results = reader.readAll();
			reader.close();
		} catch (IOException exc) {
			throw new UncheckedIOException(exc);
		}

		return results;
	}

	/**
	 * Write a result set of the given round to the csv file.
	 * 
	 * @param round
	 *            the played round
	 */
	public void writeResult(Round round) {
		CSVWriter writer;
		try {
			writer = new CSVWriter(new FileWriter(FILENAME, true));
			String[] result = new String[] { round.getSymbolicName(),
					round.getResult().toString() };
			writer.writeNext(result);
			writer.close();
		} catch (IOException exc) {
			throw new UncheckedIOException(exc);
		}
	}

	/**
	 * Cleans all results.
	 */
	public void cleanResults() {
		File file = new File(FILENAME);
		file.delete();
	}
}
