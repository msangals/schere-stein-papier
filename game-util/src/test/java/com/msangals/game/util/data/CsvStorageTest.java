package com.msangals.game.util.data;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.msangals.game.model.duel.Game;
import com.msangals.game.model.duel.Round;
import com.msangals.game.model.duel.RoundResult;
import com.msangals.game.model.player.PlayerFactory;
import com.msangals.game.model.player.PlayerType;

/**
 * Unit tests for {@link CsvStorage}.
 * 
 * @author msangals
 *
 */
public class CsvStorageTest {

	private CsvStorage storage;
	private Game game;

	@Before
	public void before() {
		storage = CsvStorage.getInstance();
		storage.cleanResults();

		PlayerFactory playerFactory = new PlayerFactory();
		game = new Game(playerFactory.getPlayer(PlayerType.DEFAULT),
				playerFactory.getPlayer(PlayerType.RANDOM));
	}

	@Test
	public void writeResultTest() {
		Round round = new Round(game, 0);
		round.setResult(RoundResult.DRAW);

		storage.writeResult(round);

		List<String[]> results = storage.getResults();

		assertThat(results.size(), is(1));
		assertThat(results.get(0)[0], is(String.format(Round.ROUND_PREFIX, 0)));
		assertThat(results.get(0)[1], is(RoundResult.DRAW.toString()));
	}

	@Test
	public void readResultsTest() {
		Round round = new Round(game, 0);
		round.setResult(RoundResult.DRAW);

		Round round1 = new Round(game, 1);
		round1.setResult(RoundResult.DEFAULT_PLAYER);

		storage.writeResult(round);
		storage.writeResult(round1);

		List<String[]> results = storage.getResults();

		assertThat(results.size(), is(2));
		assertThat(results.get(0)[0], is(String.format(Round.ROUND_PREFIX, 0)));
		assertThat(results.get(0)[1], is(RoundResult.DRAW.toString()));
		assertThat(results.get(1)[0], is(String.format(Round.ROUND_PREFIX, 1)));
		assertThat(results.get(1)[1], is(RoundResult.DEFAULT_PLAYER.toString()));
	}

}
