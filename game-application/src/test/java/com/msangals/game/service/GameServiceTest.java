package com.msangals.game.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.msangals.game.model.duel.Game;
import com.msangals.game.model.duel.Round;
import com.msangals.game.model.duel.RoundResult;
import com.msangals.game.model.duel.Symbol;
import com.msangals.game.model.player.PlayerFactory;
import com.msangals.game.model.player.PlayerType;
import com.msangals.game.service.impl.GameServiceImpl;
import com.msangals.game.util.data.CsvStorage;

/**
 * Unit tests for {@link GameService}.
 * 
 * @author msangals
 *
 */
public class GameServiceTest {

	private GameService gameService;

	@Before
	public void before() {
		gameService = new GameServiceImpl(CsvStorage.getInstance());
	}

	@Test
	public void evaluateRoundResultTest() {

		PlayerFactory playerFactory = new PlayerFactory();
		Game game = new Game(playerFactory.getPlayer(PlayerType.DEFAULT),
				playerFactory.getPlayer(PlayerType.RANDOM));
		Round round = new Round(game, 0);

		round.setDefaultPlayerSymbol(Symbol.PAPIER);
		round.setRandomPlayerSymbol(Symbol.PAPIER);
		gameService.evaluateRoundResult(round);

		assertThat(round.getResult(), is(RoundResult.DRAW));

		round.setDefaultPlayerSymbol(Symbol.PAPIER);
		round.setRandomPlayerSymbol(Symbol.SCHERE);
		gameService.evaluateRoundResult(round);

		assertThat(round.getResult(), is(RoundResult.RANDOM_PLAYER));

		round.setDefaultPlayerSymbol(Symbol.SCHERE);
		round.setRandomPlayerSymbol(Symbol.PAPIER);
		gameService.evaluateRoundResult(round);

		assertThat(round.getResult(), is(RoundResult.DEFAULT_PLAYER));

		round.setDefaultPlayerSymbol(Symbol.SCHERE);
		round.setRandomPlayerSymbol(Symbol.STEIN);
		gameService.evaluateRoundResult(round);

		assertThat(round.getResult(), is(RoundResult.RANDOM_PLAYER));

		round.setDefaultPlayerSymbol(Symbol.STEIN);
		round.setRandomPlayerSymbol(Symbol.STEIN);
		gameService.evaluateRoundResult(round);

		assertThat(round.getResult(), is(RoundResult.DRAW));
	}

	@Test
	public void evaluateGameResult() {

		PlayerFactory playerFactory = new PlayerFactory();
		Game game = new Game(playerFactory.getPlayer(PlayerType.DEFAULT),
				playerFactory.getPlayer(PlayerType.RANDOM));

		String[] resultSet = new String[] { "ROUND_0",
				RoundResult.RANDOM_PLAYER.toString() };

		gameService.evaluateGameResult(game, resultSet);

		assertThat(game.getResult().get(RoundResult.RANDOM_PLAYER), is(1));
		assertThat(game.getResult().get(RoundResult.DEFAULT_PLAYER), is(0));
		assertThat(game.getResult().get(RoundResult.DRAW), is(0));
	}

}
