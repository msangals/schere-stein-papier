package com.msangals.game.application;

import com.msangals.game.service.GameService;
import com.msangals.game.service.impl.GameServiceImpl;
import com.msangals.game.util.data.CsvStorage;

/**
 * Main application.
 * 
 * @author msangals
 *
 */
public class GameApplication {

	public static void main(String[] args) {
		GameService gameService = new GameServiceImpl(CsvStorage.getInstance());
		gameService.startGame();
	}

}
