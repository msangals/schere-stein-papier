package com.msangals.game.service;

import com.msangals.game.model.duel.Game;
import com.msangals.game.model.duel.Round;

/**
 * Game service to start the game.
 * 
 * @author msangals
 */
public interface GameService {

	/**
	 * Starts the game.
	 */
	void startGame();
	
	/**
	 * Evaluates the played round.
	 * 
	 * @param round
	 *            the played round
	 */
	void evaluateRoundResult(Round round);
	
	/**
	 * Evaluates the final result of game.
	 * 
	 * @param game
	 *            the played game
	 * @param resultSet
	 *            the result set to evaluate
	 */
	void evaluateGameResult(Game game, String[] resultSet);
}
