package com.msangals.game.service.impl;

import java.util.List;
import java.util.stream.IntStream;

import org.apache.log4j.Logger;

import com.msangals.game.model.duel.Game;
import com.msangals.game.model.duel.Round;
import com.msangals.game.model.duel.RoundResult;
import com.msangals.game.model.duel.Symbol;
import com.msangals.game.model.player.PlayerFactory;
import com.msangals.game.model.player.PlayerType;
import com.msangals.game.service.GameService;
import com.msangals.game.util.data.CsvStorage;

/**
 * This service handles the game. Implements {@link GameService}.
 * 
 * @author msangals
 *
 */
public class GameServiceImpl implements GameService {

	private static Logger logger = Logger.getLogger(GameServiceImpl.class);
	private CsvStorage storage;

	/**
	 * Constructs a new game service to handling the game.
	 * 
	 * @param storage
	 *            csv storage to store the results
	 */
	public GameServiceImpl(CsvStorage storage) {
		this.storage = storage;
	}

	/**
	 * Starts the game.
	 */
	public void startGame() {

		logger.info("Start game");

		storage.cleanResults();

		PlayerFactory playerFactory = new PlayerFactory();
		Game game = new Game(playerFactory.getPlayer(PlayerType.DEFAULT),
				playerFactory.getPlayer(PlayerType.RANDOM));

		IntStream.range(0, Game.ROUNDS_TO_PLAY).forEach(
				nr -> storage.writeResult(playRound(game, nr)));

		List<String[]> results = storage.getResults();

		results.stream().forEach(r -> evaluateGameResult(game, r));

		logger.info(RoundResult.DEFAULT_PLAYER.toString() + " "
				+ game.getResult().get(RoundResult.DEFAULT_PLAYER));
		logger.info(RoundResult.RANDOM_PLAYER.toString() + " "
				+ game.getResult().get(RoundResult.RANDOM_PLAYER));
		logger.info(RoundResult.DRAW.toString() + " "
				+ game.getResult().get(RoundResult.DRAW));

		logger.info("Game has finished");
	}

	/**
	 * Plays a round of game.
	 * 
	 * @param game
	 *            the playing game
	 * @param roundNumber
	 *            current round number
	 * @return the played round
	 */
	private Round playRound(Game game, int roundNumber) {

		Round round = new Round(game, roundNumber);

		round.decideDefaulPlayerSymbol();
		round.decideRandomPlayerSymbol();

		evaluateRoundResult(round);

		return round;
	}

	/**
	 * Evaluates the played round.
	 * 
	 * @param round
	 *            the played round
	 */
	public void evaluateRoundResult(Round round) {

		Symbol s1 = round.getDefaultPlayerSymbol();
		Symbol s2 = round.getRandomPlayerSymbol();

		if ((Symbol.SCHERE.equals(s1) && Symbol.PAPIER.equals(s2))
				|| (Symbol.STEIN.equals(s1) && Symbol.SCHERE.equals(s2))
				|| (Symbol.PAPIER.equals(s1) && Symbol.STEIN.equals(s2))) {

			// PLAYER ONE WINS
			round.setResult(RoundResult.DEFAULT_PLAYER);

		} else if ((Symbol.SCHERE.equals(s1) && Symbol.SCHERE.equals(s2))
				|| (Symbol.STEIN.equals(s1) && Symbol.STEIN.equals(s2))
				|| (Symbol.PAPIER.equals(s1) && Symbol.PAPIER.equals(s2))) {
			// DRAW
			round.setResult(RoundResult.DRAW);
		} else {
			// PLAYER TWO WINS
			round.setResult(RoundResult.RANDOM_PLAYER);
		}
	}

	/**
	 * Evaluates the final result of game.
	 * 
	 * @param game
	 *            the played game
	 * @param resultSet
	 *            the result set to evaluate
	 */
	public void evaluateGameResult(Game game, String[] resultSet) {

		int resultPlayer1;
		int resultPlayer2;
		int resultDraw;

		if (resultSet[1].equals(RoundResult.DEFAULT_PLAYER.toString())) {

			resultPlayer1 = game.getResult().get(RoundResult.DEFAULT_PLAYER);
			resultPlayer1 += 1;
			game.getResult().put(RoundResult.DEFAULT_PLAYER, resultPlayer1);

		} else if (resultSet[1].equals(RoundResult.RANDOM_PLAYER.toString())) {

			resultPlayer2 = game.getResult().get(RoundResult.RANDOM_PLAYER);
			resultPlayer2 += 1;
			game.getResult().put(RoundResult.RANDOM_PLAYER, resultPlayer2);

		} else if (resultSet[1].equals(RoundResult.DRAW.toString())) {

			resultDraw = game.getResult().get(RoundResult.DRAW);
			resultDraw += 1;
			game.getResult().put(RoundResult.DRAW, resultDraw);
		}

	}

}
